<?php

class Shape
{
	public $name = ""; // 1
	protected $width = 0; // 1
	protected $length = 0; // 1
	private $id = ""; // 1
	const SHAPE_TYPE = 1; // 2

	public function __construct($length = 0, $width = 0) // 3
	{
		$this->length = $length;
		$this->width = $width;
		$this->id = $this->uniqueID();
	}

	public function uniqueID() // 4
	{
		list($usec, $sec) = explode(" ", microtime());
	  $time = (float)$sec;
	  $random = substr(md5(mt_rand()), 0, 5);
	  return $time."_".$random;
	}

	function setName($name = "") // 5
	{
    $this->name = $name;
  }

  function getName() // 5
  {
    return $this->name;
  }

  public function calculateArea() // 6
  {
  	// empty
  }

  public static function getShapeType() // 7
  {
  	return self::SHAPE_TYPE;
  }

  public function getObject() // 8
  {
  	$temp = new stdClass();
  	$temp->name = $this->name;
  	$temp->length = $this->length;
  	$temp->width = $this->width;
  	$temp->id = $this->id;
  	return $temp;
  }
}
