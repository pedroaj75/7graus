<?php

class Rectangle extends Shape
{
	const SHAPE_TYPE = 2;

	public function __construct($length = 0, $width = 0)
	{
		parent::__construct();
		$this->length = $length;
		$this->width = $width;
	}

  public function calculateArea()
  {
  	return ($this->width * $this->length);
  }
}
