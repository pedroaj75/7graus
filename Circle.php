<?php

class Circle extends Shape
{
	const SHAPE_TYPE = 3;
	protected $radius = 0;

	public function __construct($radius = 0)
	{
		parent::__construct();
		$this->radius = $radius;
	}

  public function calculateArea()
  {
  	return (pow($this->radius, 2) * M_PI);
  }

  public function getPerimeter()
  {
  	return 2 * M_PI * $this->radius;
  }

  public function getObject() // (?)
  {
  	$temp = new stdClass();
  	$temp->name = $this->name;
  	$temp->radius = $this->radius;
  	$temp->id = $this->id;
  	return $temp;
  }
}
